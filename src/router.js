import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'



Vue.use(Router)


export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/kako-učestvovati',
      name: 'kako-učestvovati',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/KakoSudjelovati.vue')
    },
    {
      path: '/glasaj-za-pravo-na-detinjstvo',
      name: 'glasaj-za-pravo-na-detinjstvo',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/GoodCause.vue')
    },
    // {
    //   path: '/odaberi-humanitarno-udruženje',
    //   name: 'odaberi-humanitarno-udruženje',
    //   // route level code-splitting
    //   // this generates a separate chunk (about.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () => import(/* webpackChunkName: "about" */ './views/GoodCause.vue')
    // },
    {
      path: '/glasaj-za-omiljeni-crtez',
      name: 'glasaj-za-omiljeni-crtez',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/GlasujZaSvogFavorita.vue')
    },
    {
      path: '/raspored-crtanja',
      name: 'raspored-crtanja',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/RasporedCrtanja.vue')
    },
    {
      path: '/nagrade',
      name: 'nagrade',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/Nagrade.vue')
    },
    {

      path: '/objava-dobitnika',
      name: 'objava-dobitnika',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/ObjavaDobitnika.vue')
       },
      {
      path: '/pravila',
      name: 'pravila',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/Pravila.vue')

    },
    // {
    //   path: '/pravila-za-dobrobit-dece',
    //   name: 'pravila-za-dobrobit-dece',
    //   // route level code-splitting
    //   // this generates a separate chunk (about.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () => import(/* webpackChunkName: "about" */ './views/Pravila.vue')

    // },
     {
      path: '/o-sagoskatt-kolekciji',
      name: 'o-sagoskatt-kolekciji',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/SagoskatZivotinje.vue')

    },
    
    { path: "*", 
      name:'404',
    component: () => import(/* webpackChunkName: "about" */ './views/404.vue')
  },
  { path: '*', redirect: '/404' },

  //   { path: '/', component: 'home' },  
  // { path: '*', redirect: '/' }, 
  ]
})
