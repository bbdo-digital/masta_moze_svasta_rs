import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    hideCookie: false
  },
  mutations: {
    changehideCookie(state, value){
      state.hideCookie = value;
    }
  },
  actions: {

  }
})
