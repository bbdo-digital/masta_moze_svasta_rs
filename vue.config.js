module.exports = {
	 publicPath: process.env.NODE_ENV === 'production'
    ? '/masta-moze-svasta-2019'
    : '/',
  css: {
    loaderOptions: {
      sass: {
        prependData: `@import "@/style/_variables.scss";`
      }
    }
  }
};
