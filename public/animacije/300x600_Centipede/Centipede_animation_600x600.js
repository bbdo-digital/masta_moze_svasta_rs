(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol_2_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgfAJQgCgNAKgOQAJgNAMgBQALAAALAKQAMAJAAALQABALgJALQgKAMgNAAIgDABQgaAAgDgYg");
	this.shape.setTransform(155.2343,58.1176,2.6394,2.6394);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgVAYQgLgLAAgNQgBgMAKgLQAKgLAMAAQALgBAMAMQALAMAAANQABAOgJAKQgJAKgNAAQgMAAgMgMg");
	this.shape_1.setTransform(216.8792,70.3722,2.6394,2.6394);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AhpAeQgWgVgBgaQgCgbASgXQADgDAIgDIAPgIIAEAWQgCAqAQAVQAPAWAmAJQBHATAjhFQAEgIAGgSQAIgOAPgBQAOApgVAqQgWArgmAIQgVAEgTAAQhFAAg1g0g");
	this.shape_2.setTransform(173.6356,112.9387,2.6394,2.6394);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("ACHHqQgUgMgGgRQgGgQAIgRQAJgSAVgLQAMgGASgFIAhgLQAIgcgPghQgUgkgHgTIikgUQgJALgDASQgCAHgBAaIgIBkQgCATgJAKQgKAKgUACQgdAEgOAAQgZABgSgKQgUgNgFgIQgLgRAHgRQAHgRATgDQARgCAmAIQANADAeAOQAHgdgBgwQgCg7ACgTIi8g4IgvBsQgKAYgHAKQgMATgHAAIgxgBQgcgBgVgEQgRgDgNgRQgJgMgKgWQgMgeATgYQASgWAgALQApANAUAkIARAiQAVgZALgaQALgaAQhDQhGgvgXhtQhVAvgSALQgxAggmgQQgmgRgMg5QgDgQABgNQACg/AngBQAYAAAXAWQAXAXABAZQAAALgCAQIgEAeIARgGIAOgFIAegTIAdgTQAXgNAPgVQARgXALglQAEgSAYhKIhAgYQgngOgZgLQglgRgGg9QgGgzAjgrQANgQARgHQARgHAOAHQAQAIAHAPQAIAPgEASQgEAWgJAiIgQA8IBwAyQBHhYALgMQBVhfBdgxQBGgmBOARQBRARA4BHQApA0AcBGQAVAyAWBTIAKAlIADAEIBTAAIAXgBQAOAAAJABQAnAFATATQATAUAFApIAEAYQACAMgBAKQgBAWgMAPQgNAPgRACQglAFgKgvQgEgUAAg2IAEgjIh9AAIAACxQAIAJAIACQAwAPAWAxIAHANQAXAUABACQALAOgEARQgTBKgQAiQgJASgWAGQgVAFgRgKQghgVAMguQANgxA2gfQgFghgagZQgZgYgbABQgLAAgNAMIggAfQgUATgNAKQgOAKgXAKIgmASIASBAQAMAmAKAZQAMAegaArQgaArgiAEIgGAAQgPAAgRgJgACXGqQgEADgBAMQgCALADABQAHAFAJACQALACAEgDQAdgTgCgiQgGgBgGAAQgVAAgVAVgAGtEiQgNAUAGAaQAegJAJgTQAIgSgLgjIgdAjgAmbEnQAJAIAggBQgIgfgIgIQgJgIgdgEQAEAjAJAJgAAGm6QgjANgdAZIhNBFQgqAngeAhQgpAugWAmQgeAwgLA0QgOBDAmBEQAnBGA6AFQAIAAAJAGQAuAjBEAQQAOAEBsASQBEALBOgUQBBgPAog5QAng5gGhFIgFhCQgKi9hqiCQgegmgigRQgngTg0ADIgFAAQgeAAgeALgAo1gFQAEAPAaAXQAPgtgEgQQgEgRgdgJQgNAgAFARgAIagFQADAUATAPIgGhLIgNAAQgEAdABALgAm7l5QgNAYAFAYQADAKAHAEQAHAEAGgHQAUgYAJgdQADgHgCgOIgDgYQgdAPgNAYg");
	this.shape_3.setTransform(159.2116,131.7663,2.6394,2.6394);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_2_Layer_1, null, null);


(lib.Symbol_1_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgUATIATglQAMAFAFAEQAHAFgDAJQgDAMgLACIgaAAg");
	this.shape.setTransform(136.5596,120.5826,2.6394,2.6394);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AAAAWQgJgBgHgEQgDgCgDgJQgCgIACgFQAFgMAMgCQAHgBAPAEQAPAagSALQgEADgIAAIgCAAg");
	this.shape_1.setTransform(66.1885,130.1335,2.6394,2.6394);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("Ag+AUIgcgLQgKgEAAgFQAAgGAJgNIAGAAQAZASAiABQAWAAAmgHQAWgEAtgOQgDAcgfAGQg8APgoACIgCAAQgLAAgQgGg");
	this.shape_2.setTransform(103.2163,146.2559,2.6394,2.6394);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgbFAQgbAAgogLQghgKgfgoQgzhGAKhiQAIhJA1g5IBPhTIgRg1IgYAJIgvAWQgcANgUAGQggALgmgGQgVgDgKgQQgKgQAEgUQADgPANgJQBHgtBEACQAdABAUAHQAKADAIgCQAGgCAJgHQAXgUAzgmQANgJAKgBQBLgIAlgCQASAAAJAJQAIAJAAAUQABAggQAXQgPAYgcAJQgvAOhKAQQgIACgOAAIgbgBIAOAvIBHADQApABAbAEQBfAPA4BRQAyBHgBBbQAAAPgEAHQgSArgVAaQgaAggkAQQg8AahDAEQgwACgbAAIgYgBgAgXhZQgUADgUAPIghAdQg8AxgOBMQgPBPAqBKQAXAoApAMQA1APA9gHIANABQBIAAA+gZQAsgSAcgsQAdgsgEgtQgHhPgyg6Qgvg2hDgRQgdgHglAAQgfAAgiAFgAkFi3QgKAIgDADQgGAIAEAJQACAEAJADIAPAGIAGAAQAigBAmgSQAOgHA0gfIgFgGIgDgDQgXgDgUAAQg8AAgsAcgAAskXQgzARgeAvIAIAVQAYACAxgJQAxgJASgKQAdgQAHgcIABgOIgBgRQhJAGgeAKg");
	this.shape_3.setTransform(80.1713,84.6173,2.6394,2.6394);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_1_Layer_1, null, null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_2_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(159.2,131.8,1,1,0,0,0,159.2,131.8);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(0,0,318.5,263.6), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_1_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(80.2,84.6,1,1,0,0,0,80.2,84.6);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(0,0,160.4,169.3), null);


(lib.Scene_1_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(120,202.1,1,1,0,0,0,80.2,84.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:321.05},29,cjs.Ease.quadOut).to({y:202.1},30,cjs.Ease.backOut).wait(14).to({y:321.05},29,cjs.Ease.quadOut).to({y:202.1},30,cjs.Ease.backOut).wait(18).to({y:321.05},29,cjs.Ease.quadOut).to({y:202.1},30,cjs.Ease.backOut).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(402,351.9,1,1,0,0,0,159.2,131.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(58).to({rotation:360},15,cjs.Ease.backOut).wait(59).to({rotation:720},15,cjs.Ease.backOut).wait(48).to({rotation:1080},14,cjs.Ease.backOut).wait(1));

}).prototype = p = new cjs.MovieClip();


// stage content:
(lib.Centipede_animation_600x600 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	this.___GetDepth___ = function(obj) {
		var depth = obj.depth;
		var cameraObj = this.___camera___instance;
		if(cameraObj && cameraObj.depth && obj.isAttachedToCamera)
		{
			depth += depth + cameraObj.depth;
		}
		return depth;
		}
	this.___needSorting___ = function() {
		for (var i = 0; i < this.getNumChildren() - 1; i++)
		{
			var prevDepth = this.___GetDepth___(this.getChildAt(i));
			var nextDepth = this.___GetDepth___(this.getChildAt(i + 1));
			if (prevDepth < nextDepth)
				return true;
		}
		return false;
	}
	this.___sortFunction___ = function(obj1, obj2) {
		return (this.exportRoot.___GetDepth___(obj2) - this.exportRoot.___GetDepth___(obj1));
	}
	this.on('tick', function (event){
		var curTimeline = event.currentTarget;
		if (curTimeline.___needSorting___()){
			this.sortChildren(curTimeline.___sortFunction___);
		}
	});

	// timeline functions:
	this.frame_209 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(209).call(this.frame_209).wait(1));

	// Layer_2_obj_
	this.Layer_2 = new lib.Scene_1_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(402,351.9,1,1,0,0,0,402,351.9);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 0
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(210));

	// Layer_3_obj_
	this.Layer_3 = new lib.Scene_1_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(120,202.1,1,1,0,0,0,120,202.1);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 1
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(210));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(339.8,405.6,221.49999999999994,105.79999999999995);
// library properties:
lib.properties = {
	id: 'D009AEEBFD14490F8571992ACF95BB47',
	width: 600,
	height: 600,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D009AEEBFD14490F8571992ACF95BB47'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


// Layer depth API : 

AdobeAn.Layer = new function() {
	this.getLayerZDepth = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth; else 0;";
		return eval(script);
	}
	this.setLayerZDepth = function(timeline, layerName, zDepth)
	{
		const MAX_zDepth = 10000;
		const MIN_zDepth = -5000;
		if(zDepth > MAX_zDepth)
			zDepth = MAX_zDepth;
		else if(zDepth < MIN_zDepth)
			zDepth = MIN_zDepth;
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth = " + zDepth + ";";
		eval(script);
	}
	this.removeLayer = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline.removeChild(timeline." + layerName + ");";
		eval(script);
	}
	this.addNewLayer = function(timeline, layerName, zDepth)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		zDepth = typeof zDepth !== 'undefined' ? zDepth : 0;
		var layer = new createjs.MovieClip();
		layer.name = layerName;
		layer.depth = zDepth;
		layer.layerIndex = 0;
		timeline.addChild(layer);
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;